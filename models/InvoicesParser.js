var cls = InvoicesParser.prototype;
var Invoice = require('./Invoice');
var Config = require('./Config');

/**
 * Parses a single input ASCII file.
 * @constructor
 * @param {string} content - the content of the input file as one string.
 */
function InvoicesParser(content) {
    this._invoices = [];
    var lines_per_invoice = Config.digit.height;
    var content_lines = content.replace("\r\n", "\n").split("\n");
    
    // Parse each group of ascii lines to a parsed Invoice instance
    for (var i = 0; i < content_lines.length; i += (lines_per_invoice + Config.invoice.vertical_space)) {
        if (typeof content_lines[i+lines_per_invoice] != 'undefined') {
            this._invoices.push(new Invoice(content_lines.slice(i, i+lines_per_invoice)));
        }
    }
}

cls.get_invoices = function() {
    var invoices = [];
    for (var i in this._invoices) {
        invoices.push(this._invoices[i].get_number());
    }
    return invoices;
};

module.exports = InvoicesParser;
