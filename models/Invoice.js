var cls = Invoice.prototype;
var Digit = require('./Digit');
var Config = require('./Config');

/**
 * Represents a single invoice number.
 * @constructor
 * @param {array} lines - Each element in the array is a line from the input ASCII file.
 */
function Invoice(lines) {
    this._digits = [];
    // Parse each digit to a parsed Digit instance
    for (var i = 0; i < lines[0].length; i += Config.digit.width) {
        // All lines per digit will be concatenated to one string
        var digit_text = '';
        for (var j = 0; j < Config.digit.height; j++) {
            digit_text += lines[j].slice(i, i+Config.digit.width);
        }
        this._digits.push(new Digit(digit_text));
    }
}

cls.get_number = function() {
    var digits = '';
    var illegal = false;
    for (var d in this._digits) {
        var digit = this._digits[d].get_parsed_digit();
        if (digit !== false) {
            digits += digit;
        } else {
            digits += '?';
            illegal = true;
        }
    }
    if (illegal) {
        digits += ' ILLEGAL';
    }
    return digits;
};

module.exports = Invoice;
