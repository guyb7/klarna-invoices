var cls = FilesReaderWriter.prototype;
var fs = require('fs');

/**
 * Used to read and write files, and parse arguments.
 * @constructor
 * @param {array} args - Arguments passed to the script (without the `node` command and the file name)
 */
function FilesReaderWriter(args) {
    this._files = [];
    this._output_file = false;
    for (var i = 0; i < args.length; i++) {
        if (typeof args[i+1] == 'undefined' || args[i+1].length < 0) {
            continue;
        }
        switch (args[i]) {
            case '-i':
                // Add specific file
                this._files.push(args[i+1]);
                break;
            case '-o':
                this._output_file = args[i+1];
/*
            case '-d':
                // Add all files in that directory - not recursive
                try {
                    var dir = args[i+1];
                    var files = fs.readdirSync(dir);
                    for (var f in files) {
                        if(!fs.statSync(dir + '/' + files[f]).isDirectory()){
                            this._files.push(dir + '/' + files[f]);
                        }
                    }
                } catch(err) {
                    console.log(err.message);
                }
                break;
*/
            default:
        }
    }
    if (this._output_file === false) {
        console.log('No output file specified');
        this.print_usage();
        process.exit(1);
    } else if (this._files.length === 0) {
        console.log('No files to read');
        this.print_usage();
        process.exit(2);
    }
}

cls.read_file = function(file, callback) {
    fs.readFile(file, 'utf8', function (err, content) {
        if (err) throw err;
        callback(content);
    });
};

/** Call the callback function for each input file. */
cls.iterate = function(callback) {
    for (var f in this._files) {
        // Parameters: callback(filename, content)
        var filename = this._files[f];
        this.read_file(filename, function(content){
            callback(filename, content);
        });
    }
};

cls.print_usage = function() {
    console.log('Usage: app.js -i input.txt -o output.txt');
};

cls.write = function(content) {
    var destination = this._output_file;
    fs.writeFile(destination, content, function (err) {
        if (err) throw err;
        console.log('Output was saved to: ' + destination);
    });
};

module.exports = FilesReaderWriter;
