var cls = Digit.prototype;

/**
 * Converts ASCII format digits to a decimal integer.
 * @constructor
 */
function Digit(ascii) {
    this._digit = this.parse_digit(ascii);
}

cls.parse_digit = function(ascii) {
    switch (ascii) {
        case  ' _ '
            + '| |'
            + '|_|':
            return 0;
        case  '   '
            + '  |'
            + '  |':
            return 1;
        case  ' _ '
            + ' _|'
            + '|_ ':
            return 2;
        case  ' _ '
            + ' _|'
            + ' _|':
            return 3;
        case  '   '
            + '|_|'
            + '  |':
            return 4;
        case  ' _ '
            + '|_ '
            + ' _|':
            return 5;
        case  ' _ '
            + '|_ '
            + '|_|':
            return 6;
        case  ' _ '
            + '  |'
            + '  |':
            return 7;
        case  ' _ '
            + '|_|'
            + '|_|':
            return 8;
        case  ' _ '
            + '|_|'
            + ' _|':
            return 9;
        default:
            return false;
    }
};

cls.get_parsed_digit = function() {
    return this._digit;
};

module.exports = Digit;
