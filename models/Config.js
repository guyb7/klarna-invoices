/** A global config file used by various classes */
module.exports = {
    digit: {
        width: 3,
        height: 3
    },
    invoice: {
        vertical_space: 1
    }
};
