#!/usr/bin/env node

/** A command line tool that receives and parses files with ASCII-art digits. */

var FilesReaderWriter = require('./models/FilesReaderWriter');
var InvoicesParser = require('./models/InvoicesParser');

var args = process.argv.slice(2);
var files = new FilesReaderWriter(args);

// For each input file: read, parse, write
files.iterate(function(file, content){
    var parser = new InvoicesParser(content);
    var invoices = parser.get_invoices();
    invoices.push(''); // Adding an empty item at the end to meet the expected output format
    files.write(invoices.join("\n"));
});
