#!/usr/bin/env node

var fs = require('fs');

// Init Jasmine
var jasmine = require('./jasmine-2.4.0/jasmine.js');
var jasmineConsole = require('./jasmine-2.4.0/console.js');
jasmine = jasmine.core(jasmine);
jasmineConsole.console(jasmineConsole, jasmine);

// Set up the console logger
jasmine.getEnv().addReporter(new jasmine.ConsoleReporter({ print: console.log }));

// Avoid putting jasmine.getEnv() everywhere
var describe = jasmine.getEnv().describe;
var it = jasmine.getEnv().it;
var expect = jasmine.getEnv().expect;

// A function to execute shell commands
function run_cmd(cmd, args, callBack ) {
    var spawn = require('child_process').spawn;
    var child = spawn(cmd, args);
    var resp = "";
    child.stdout.on('data', function (buffer) { resp += buffer.toString() });
    child.stdout.on('end', function() { callBack (resp) });
}

// The tests
describe('Compare precompiled files', function() {
    it('should match user-story-1', function(done) {
        run_cmd( 'node', ['app.js', '-i', 'tests/test-cases/input_user_story_1.txt', '-o', 'tests/tmp/output_user_story_1.txt'], function() {
            var test_output = fs.readFileSync('./tests/tmp/output_user_story_1.txt', "utf8").replace("\r\n", "\n");
            var correct_output = fs.readFileSync('./tests/test-cases/output_user_story_1.txt', "utf8").replace("\r\n", "\n");
            expect(test_output).toEqual(correct_output);
            // Delete output file
            fs.unlinkSync('./tests/tmp/output_user_story_1.txt');
            done();
        });
    });
    
    it('should match user-story-2', function(done) {
        run_cmd( 'node', ['app.js', '-i', 'tests/test-cases/input_user_story_2.txt', '-o', 'tests/tmp/output_user_story_2.txt'], function() {
            var test_output = fs.readFileSync('./tests/tmp/output_user_story_2.txt', "utf8").replace("\r\n", "\n");
            var correct_output = fs.readFileSync('./tests/test-cases/output_user_story_2.txt', "utf8").replace("\r\n", "\n");
            expect(test_output).toEqual(correct_output);
            // Delete output file
            fs.unlinkSync('./tests/tmp/output_user_story_2.txt');
            done();
        });
    });
});

describe('Digit class', function() {
    var Digit = require('../models/Digit');
    it('tests for correct input', function() {
        var digit = new Digit('     |  |');
        expect(digit.get_parsed_digit()).toEqual(1);
    });
    
    it('tests for incorrect input', function() {
        var digit = new Digit('xxx');
        expect(digit.get_parsed_digit()).toEqual(false);
    });
});

describe('Invoice class', function() {
    var Invoice = require('../models/Invoice');
    var Config = require('../models/Config');
    it('tests for correct input', function() {
        var invoice = new Invoice(['    _  _ ','  | _| _|','  ||_  _|']);
        expect(invoice.get_number()).toEqual('123');
    });
    
    it('tests for corrupted input', function() {
        var invoice = new Invoice(['    _  _ ','  | _  _|','  ||_  _|']);
        expect(invoice.get_number()).toEqual('1?3 ILLEGAL');
    });
});

describe('InvoicesParser class', function() {
    var InvoicesParser = require('../models/InvoicesParser');
    var Config = require('../models/Config');
    it('tests for correct and corrupted input', function() {
        var content = "    _  _ \n  | _| _|\n  ||_  _|\n\n    _  _ \n  | _  _|\n  ||_  _|\n";
        var parser = new InvoicesParser(content);
        var invoices = parser.get_invoices();
        expect(invoices).toEqual(['123', '1?3 ILLEGAL']);
    });
});


// Kick off execution
jasmine.getEnv().execute();
