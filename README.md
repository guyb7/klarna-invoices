# KlarnaInvoices.js
A command-line tool to parse ASCII invoices and ouput a sane decimal output.

## Installation
1. Install [Node.js](https://nodejs.org/en/). This version was tested on v4.2.3.
2. Clone this repo to a desired location.

## Usage
Run app.js with 2 parameters: `-i` for the input file location, and `-o` for the location to write the output to (existing files will be overwritten!).
```
#!bash
node app.js -i input_user_story_1.txt -o output_user_story_1.txt
```

## Input format & Output
Input files contain ASCII digits (3x3 characters):

```
#!bash
    _  _     _  _  _  _  _ 
  | _| _||_||_ |_   ||_||_|
  ||_  _|  | _||_|  ||_| _|
```


Each line will be parsed to decimal format:
```
#!bash
123456789
```
Corrupted digits will be replaced with `?`, and a `ILLEGAL` label will be appended to the end of the line.

## Running Tests
The tests are written over Jasmine and can be run with the following command:
```
#!bash
node tests/run-tests.js
```

**Important:**

1. Run the command from the base directory as the tests expect this current-working-directory.
2. In some installation of node the execution command will be `nodejs` or something other than `node`. In this case update the tests in `run-tests.js` to use the correct command.

A successful run looks something like this:
```
#!bash
Started
.
.
2 specs, 0 failures

Finished in 0 seconds
```

## License
MIT